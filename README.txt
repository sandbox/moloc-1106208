// $Id$

The Questionary-Level module of the EthifyGame provides up to 10 configurable
questions, which can be answered with yes or no. This level only allowes the
correct answer, which is configureable in the admin settings.

The Questionary-Level will also throw an event, if the level is done. Therefore
other modules can react on these events.

If the level is completed, the user is not able to change the answers (but the
user can still access the questions-page).

Functionality:
 + up to 10 configureable questions
 + executes an event "level done"
 
Requirements
------------
 + Drupal 6
 + Modules
   - ethifygame
   - rules (optional)

Installation
------------
1) Copy the Questionary directory to the modules folder in your installation.

2) Enable the module using Administer -> Modules (/admin/build/modules)
   (Note: EthifyGame Module must be installed/enabled first!)

Configuration
-------------

1) Configure default settings for the level
   Administer -> Site Configuration -> Ethifygame -> Questionary
  (/admin/settings/ethifygame/level/questionary)

2) Add Triggered Rules, if needed (e.g. give ethify points on level done)
   Administer -> Rules -> Triggered rules (admin/rules/trigger)

Using the module
----------------

After enabling the module, the questionary level is active and can be played.
Ethifygame will automatically set the level-number on the first time, this
level is enabled, as the last level. You can modify the level-order in the
ethifygame settings (see /admin/settings/ethifygame).