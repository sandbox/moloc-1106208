<?php
// $Id$

/**
 * @file
 * Callback form function of the ethifygame_level_questionary module.
 */

include_once dirname(__FILE__) .'/ethifygame_level_questionary_settings.inc';


function _ethifygame_level_questionary_form() {
  
  // load css
  static $setup = TRUE;
  if ($setup) {
    $setup = FALSE;
    drupal_add_css(drupal_get_path('module', 'ethifygame_level_questionary') .'/theme/questionary.css');
    drupal_add_js(drupal_get_path('module', 'ethifygame_level_questionary') .'/theme/questionary.js');
  }
  
  $form = array();
  
  $level = ethifygame::factory_get_level_by_name('questionary');
  $level_played = $level->is_level_played();
  if (!$level->is_first_level()) {
    $form['a_prev'] = array(
      '#type' => 'markup',
      '#value' => ethifygame_get_markup_go('prev', 'questionary'),
    );
  }
  if ($level_played) {
    $form['a_next'] = array(
      '#type' => 'markup',
      '#value' => ethifygame_get_markup_go('next', 'questionary'),
    );
  }
  
  $i = 0;
  while( $i < ETHIFYGAME_LEVEL_QUESTIONARY_QUESTIONS_COUNT ) {
    $question = variable_get('ethifygame_level_questionary_question_'.$i, '');
    
    if( !empty($question) ) {
      $form['eg_question_'.$i] = array(
        '#title' => '',
        '#type' => 'fieldset',
      );
      $form['eg_question_'.$i]['label'] = array(
        '#type' => 'markup',
        '#value' => t($question),
      );
      if (!$level_played) {
        $form['eg_question_'.$i]['eg_answer_'.$i] = array(
          '#type' => 'radios',
          '#attributes' => array('class' => 'eg_lvl_questionary_show_btn'),
          '#options' => array(
            '1' => '', //t(variable_get('ethifygame_level_questionary_answer_' . $i . '_1', 'Ja')),
            '2' => '', //t(variable_get('ethifygame_level_questionary_answer_' . $i . '_2', 'Nein')),
          ),
        );
      }
      else {
        $form['eg_question_'.$i]['eg_answer_'.$i] = array(
          '#type' => 'radios',
          '#default_value' => variable_get('ethifygame_level_questionary_solution_' . $i, '1'),
          '#attributes' => array('class' => 'eg_lvl_questionary_show_btn'),
          '#options' => array(
            '1' => '', //t(variable_get('ethifygame_level_questionary_answer_' . $i . '_1', 'Ja')),
            '2' => '', //t(variable_get('ethifygame_level_questionary_answer_' . $i . '_2', 'Nein')),
          ),
        );
      }
    }
    $i++;
  }
  // set the count of questions possible
  $form['eg_question_count'] = array(
    '#type' => 'hidden',
    '#value' => ETHIFYGAME_LEVEL_QUESTIONARY_QUESTIONS_COUNT,
  );
  if (!$level_played) {
    // If we have not played this level, show submit button.
    $form['eg_question_enable'] = array(
      '#type' => 'hidden',
      '#value' => true,
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Submit Answer'),
    );
  }

  return $form;
}

/**
 * Implementation of hook_form_validate().
 */
function _ethifygame_level_questionary_form_validate($form, &$form_state) {
  // 1 == ja
  // 2 == nein
  $valid_entries = array('1', '2');
  
  
  $i = 0;
  $questionnr = 0;
  while ($i < ETHIFYGAME_LEVEL_QUESTIONARY_QUESTIONS_COUNT) {
    $question = variable_get('ethifygame_level_questionary_question_' . $i, '');
    
    if (!empty($question)) {
      $questionnr++;
      // check, if the answer is correct
      if (!in_array($form_state['values']['eg_answer_' . $i], $valid_entries, TRUE)) {
        form_set_error('eg_answer_' . $i, t('Answer of question !question-nr is required.', array('!question-nr' => $questionnr)));
      }
      else {
        $answer_solution = variable_get('ethifygame_level_questionary_solution_' . $i, '1');
        $answer_user = $form_state['values']['eg_answer_' . $i];
        if ($answer_user != $answer_solution) {
          form_set_error('eg_answer_' . $i, t('Please reconsider your answer for question !question-nr.', array('!question-nr' => $questionnr)));
        }
      }
    }
    $i++;
  }
}

/**
 * Implementaiton of hook_form_submit().
 */
function _ethifygame_level_questionary_form_submit($form, &$form_state) {
  // mark level done
  $level = ethifygame::factory_get_level_by_name('questionary');
  if( !$level->is_level_played() && $result = $level->mark_level() ) {
    if (module_exists('rules')) {
      global $user;
      $event_name = 'eg_lvl_questionary_done';
      rules_invoke_event($event_name, $user);
    }
  }
  
  // goto next level
  $level->goto_next();
}

