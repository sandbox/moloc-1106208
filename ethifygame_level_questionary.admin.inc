<?php
// $Id$

/**
 * @file
 * Callback admin settings form function of the eg-questionary module.
 */

include_once dirname(__FILE__) .'/ethifygame_level_questionary_settings.inc';

/**
 * Builds the form for configuring the settings
 * of the questionary module.
 *
 * @ingroup forms
 * @see system_settings_form().
 */
function _ethifygame_level_questionary_admin_settings() {
  $default_questions = ethifygame_level_questionary_get_default_questions();
  $question = '';
  $questions_count = count($default_questions);
  
  $i = 0;
  $form = array();
  while ($i < ETHIFYGAME_LEVEL_QUESTIONARY_QUESTIONS_COUNT) {
    if ($i < $questions_count) {
      $question = $default_questions[$i];
    }
    else {
      $question = '';
    }
    _ethifygame_level_questionary_create_form_items(&$form, $i, $question);
    $i++;
  }
  
  return system_settings_form(&$form);
}

/**
 * Adds a question box to the given form with the given default-question
 * @param $form
 * @param int $nr A unique number to identify the question-box
 * @param string $default_question
 */
function _ethifygame_level_questionary_create_form_items(&$form, $nr, $default_question) {
  $form['ethifygame_level_questionary_question_' . $nr] = array(
    '#type' => 'textfield',
    '#title' => t('Question'),
    '#default_value' => variable_get('ethifygame_level_questionary_question_'. $nr, $default_question),
  );
  $form['ethifygame_level_questionary_solution_' . $nr] = array(
    '#type' => 'radios',
    '#title' => t('Correct answer'),
    '#default_value' => variable_get('ethifygame_level_questionary_solution_' . $nr, '1'),
    '#options' => array(
      '1' => t('Yes'),
      '2' => t('No'),
    ),
  );
//  $form['ethifygame_level_questionary_answer_'.$nr.'_1'] = array(
//    '#type' => 'textfield',
//    '#title' => t('Answer 1'),
//    '#default_value' => variable_get('ethifygame_level_questionary_answer_'.$nr.'_1', 'Ja'),
//  );
//  $form['ethifygame_level_questionary_answer_'.$nr.'_2'] = array(
//    '#type' => 'textfield',
//    '#title' => t('Answer 2'),
//    '#default_value' => variable_get('ethifygame_level_questionary_answer_'.$nr.'_2', 'Nein'),
//  );
//  $form['ethifygame_level_questionary_solution_'.$nr] = array(
//    '#type' => 'textfield',
//    '#title' => t('Correct solution'),
//    '#description' => t('Either answer "1" or "2". Write just the number.'),
//    '#default_value' => variable_get('ethifygame_level_questionary_solution_'.$nr, '1'),
//  );
}

