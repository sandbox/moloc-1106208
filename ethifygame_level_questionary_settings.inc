<?php
// $Id$

/**
 * @file
 * Default Settings for the questionary-level.
 */

define('ETHIFYGAME_LEVEL_QUESTIONARY_QUESTIONS_COUNT', 10);

function ethifygame_level_questionary_get_default_questions() {
  return array(
  	'Hast du ein Gewissen?',
    'Bist du fair?',
    'Ist dir die Umwelt wichtig?',
  );
}
