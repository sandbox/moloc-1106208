// $Id$

$(document).ready(function(){
  var e = [];
  var p = [];

  function setChangeHandler(radio_index, el_index) {
    e[radio_index][el_index].change(function() { setSelectedRadio(radio_index, el_index, true); });
  }
  function setFocusHandler(radio_index, el_index) {
    /* blur, otherwise we will see the focus of the input-elements (msie fix) */
    e[radio_index][el_index].focus(function() { this.blur(); });
  }
  function setClickHandler(radio_index, el_index) {
	  var clkHandler = function() {
		  setSelectedRadio(radio_index, el_index, true);
		  e[radio_index][el_index].unbind('click', clkHandler);
	  };
	  e[radio_index][el_index].click(clkHandler);
  }

  /* add/remove select/error class for css styling */
  function setSelectedRadio(radio_index, el_index, setAll) {
    var next_el = 3 - el_index; // => (1 - (el_index - 1)) + 1
    var el_type = (el_index == 1 ? "yes" : "no");
    var next_el_type = (next_el == 2 ? "no" : "yes");
    var tmp = e[radio_index][el_index];

    if( e[radio_index][el_index].attr("checked") ) {
      p[radio_index][el_index].addClass("eg-" + el_type + "-active");
      p[radio_index][el_index].removeClass("eg-" + el_type + "-error");
      p[radio_index][el_index].removeClass("eg-" + el_type + "-inactive");
      if(setAll)  {
    	p[radio_index][next_el].addClass("eg-" + next_el_type + "-inactive");
    	p[radio_index][next_el].removeClass("eg-" + next_el_type + "-active");
        p[radio_index][next_el].removeClass("eg-" + next_el_type + "-error");
      }
    } else {
      p[radio_index][el_index].addClass("eg-" + el_type + "-inactive");
      p[radio_index][el_index].removeClass("eg-" + el_type + "-active");
      p[radio_index][el_index].removeClass("eg-" + el_type + "-error");
      if(setAll) {
      	p[radio_index][next_el].addClass("eg-" + next_el_type + "-active");
      	p[radio_index][next_el].removeClass("eg-" + next_el_type + "-inactive");
        p[radio_index][next_el].removeClass("eg-" + next_el_type + "-error");
      }
    }
  }
  
  var max_elements = parseInt($('#edit-eg-question-count').val());
  if (isNaN(max_elements)) {
	  max_elements = 0;
  }
  
  var enable_changes = !!$('#edit-eg-question-enable').val();
  
  /* init state and set event listener */
  for (var i = 0; i < max_elements; i++) {
    e[i] = [];
    p[i] = [];

    for (var k = 1; k <= 2; k++) {
      var x;
      x = $('#edit-eg-answer-' + i + "-" + k);
      e[i][k] = x;
      p[i][k] = e[i][k].parent()

      setSelectedRadio(i, k, false);
      if (enable_changes) {
  	    setChangeHandler(i, k);
  	    setFocusHandler(i, k);
	    if (e[i][k].hasClass("error")) {
	      if (k == 1) {
	    	p[i][k].addClass("eg-yes-error");
	      }
	      else {
	    	p[i][k].addClass("eg-no-error");
	      }
	      setClickHandler(i, k);
	    }
      }
      if( $.browser.msie ) {
    	  /* just for msie: change visibility to delegate radio-events */
    	  e[i][k].addClass("eg-msie");
      }
    }
  }
});